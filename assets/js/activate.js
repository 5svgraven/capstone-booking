let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");
let token = localStorage.getItem("token");

fetch(`https://capstonebookingapi.herokuapp.com/api/courses/${courseId}`, {
    method: "PUT",
    headers: {
        Authorization: `Bearer ${token}`,
    },
})
    .then((res) => {
        return res.json();
    })
    .then((data) => {
        if (data) {
            alert("Course Activated Successfully");
        } else {
            alert("Course Activation Failed");
        }
    });