
let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");
let token = localStorage.getItem("token");
let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let container = document.querySelector("#userContainer");
let student = document.getElementById("stud")
let isAdmin = localStorage.getItem("isAdmin")

if (token === null) {
  register.innerHTML = `<a href="./register.html" class="nav-link active"> Register </a>`
  login.innerHTML = `<a href="./login.html" class="nav-link active"> Log in </a>`
} else {
  if (isAdmin == "true") {
    profile.innerHTML = null
    logout.innerHTML = `<a href="./logout.html" class="nav-link active"> Log Out </a>`

  } else {
    profile.innerHTML = `<a href="./profile.html" class="nav-link active"> Profile </a>`
    logout.innerHTML = `<a href="./logout.html" class="nav-link active"> Log Out </a>`
  }
}



fetch(`https://capstonebookingapi.herokuapp.com/api/courses/${courseId}`)
  .then((res) => {
    return res.json();
  })
  .then((data) => {
    courseName.innerHTML = data.name;
    courseDesc.innerHTML = data.description;
    coursePrice.innerHTML = data.price;


    if (adminUser === "true") {
      student.innerHTML = `<h1 id="" class="text-center">Students Enrolled</h1>`
      let userData
      let userData2 = data.enrollees
      userData2.map((user) => {
        fetch(`https://capstonebookingapi.herokuapp.com/api/users/${user.userId}`)
          .then((res) => res.json())
          .then((data) => {
            userData = data
            userData = (`
            <div class="col-md-6 my-3">              
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title">${userData.firstname} ${userData.lastname} </h5>                                    
                </div>
              </div>
            </div>
            `)
            container.innerHTML += userData
          })
        let container = document.querySelector("#userContainer");
      })
    }
    else {
      let enrollCourse = document.querySelector("#enrollCourse");
      enrollCourse.innerHTML = `<button id="enrollCourse" class="btn btn-block btn-primary"> Enroll </button>`;
      document.querySelector("#enrollCourse").addEventListener("click", () => {
        if (token === null) {
          window.location.replace("./login.html");
        } else {
          fetch("https://capstonebookingapi.herokuapp.com/api/users/enroll", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify({
              courseId: courseId
            }),
          })
            .then((res) => {
              return res.json();
            })
            .then((data) => {
              if (data) {
                alert("User Enrolled");
              } else {
                alert("Enrollment Failed");
              }
            });
        }
      })
    }

  });

