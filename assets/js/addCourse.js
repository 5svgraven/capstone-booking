let formSubmit = document.querySelector("#createCourse");

formSubmit.addEventListener("submit", (e) => {
  e.preventDefault();

  let courseName = document.querySelector("#courseName").value;
  let description = document.querySelector("#courseDescription").value;
  let price = document.querySelector("#coursePrice").value;

  let token = localStorage.getItem("token");

  fetch("https://capstonebookingapi.herokuapp.com/api/courses", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",

      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({
      name: courseName,
      description: description,
      price: price,
    }),
  })
    .then((res) => {
      return res.json();
    })
    .then((data) => {
      if (data) {
        window.location.replace("./courses.html");
      } else {
        alert("Course not added");
      }
    });
});
