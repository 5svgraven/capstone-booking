let isAdmin = localStorage.getItem("isAdmin");
let modalButton = document.querySelector("#adminButton");
let token = localStorage.getItem("token")
if (token === null) {
  register.innerHTML = `<a href="./register.html" class="nav-link active"> Register </a>`
  login.innerHTML = `<a href="./login.html" class="nav-link active"> Log in </a>`
} else {
  if (isAdmin == "true") {
    profile.innerHTML = null
    logout.innerHTML = `<a href="./logout.html" class="nav-link active"> Log Out </a>`

  } else {

    profile.innerHTML = `<a href="./profile.html" class="nav-link active"> Profile </a>`
    logout.innerHTML = `<a href="./logout.html" class="nav-link active"> Log Out </a>`
  }
}



let cardFooter;
let cardActive;

if (adminUser == "false" || !adminUser) {
  modalButton.innerHTML = null;

} else {

  modalButton.innerHTML = `
			<div class="col-md-2 offset-md-10">
				<a href="./addCourse.html" class="btn btn-block btn-primary">
					Add Course
				</a>
			</div>
		`;
}

if (isAdmin == "true") {
  fetch("https://capstonebookingapi.herokuapp.com/api/courses")
    .then((res) => res.json())
    .then((data) => {
      let courseData;

      if (data.length < 1) {
        courseData = "No courses available";
      } else {
        courseData = data
          .map((course) => {
            if (course.isActive == false) {
              cardActive = `<a href="./update.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block">Activate</a>`;
            } else {
              cardActive = `<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-danger text-white btn-block">Archive</a>`;
            }
            cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-secondary text-white btn-block">View Course</a>`;


            return `
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">${course.name}</h5>
							<p class="card-text text-left">${course.description}</p>
							<p class="card-text text-right">${course.price}</p>
            </div>
            <div class="card-footer">
              ${cardFooter}              
            </div>
             <div class="card-footer">
              ${cardActive}              
						</div>
					</div>
				</div>
				`;
          })
          .join("");
      }
      let container = document.querySelector("#coursesContainer");
      container.innerHTML = courseData;
    });
} else {
  fetch("https://capstonebookingapi.herokuapp.com/api/courses/active")
    .then((res) => res.json())
    .then((data) => {
      let courseData;

      if (data.length < 1) {
        courseData = "No courses available";
      } else {
        courseData = data
          .map((course) => {

            cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block">Go to Course</a>`;
            cardActive = `<a href="./courses.html" class="btn btn-primary text-white btn-block invisible"></a>`;


            return `
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">${course.name}</h5>
							<p class="card-text text-left">${course.description}</p>
							<p class="card-text text-right">${course.price}</p>
            </div>
            <div class="card-footer">
							${cardFooter}
						</div>
						<div class="card-footer">
							${cardActive}
						</div>
					</div>
				</div>
				`;
          })
          .join("");
      }
      let container = document.querySelector("#coursesContainer");
      container.innerHTML = courseData;
    });
}



