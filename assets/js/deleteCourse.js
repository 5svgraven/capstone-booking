let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");
let token = localStorage.getItem("token");

fetch(`https://capstonebookingapi.herokuapp.com/api/courses/${courseId}`, {
    method: "DELETE",
    headers: {
        Authorization: `Bearer ${token}`,
    },
})
    .then((res) => {
        return res.json();
    })
    .then((data) => {
        if (data) {
            alert("Course Archive Successfully");
        } else {
            alert("Course Archive Failed");
        }
    });