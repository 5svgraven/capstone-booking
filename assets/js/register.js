let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let email = document.querySelector("#userEmail").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value

	if ((password1 !== "" && password2 !== "") && (password1 === password2) && (mobileNo.length == 11)) {
		fetch("https://capstonebookingapi.herokuapp.com/api/users/email-exists", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
			.then(response => response.json())
			.then(data => {
				if (data === false) {
					fetch("https://capstonebookingapi.herokuapp.com/api/users", {
						method: "POST",
						headers: {
							"Content-Type": "application/json"
						},
						body: JSON.stringify({
							firstname: firstName,
							lastname: lastName,
							email: email,
							password: password1,
							mobileNo: mobileNo
						})
					})
						.then(response => response.json())
						.then(data => {
							if (data) {
								alert("Registration successful")
							} else {
								alert("Registration went wrong")
							}
						})
				} else {
					alert("Email already exists")
				}
			})
	} else {
		alert("Invalid inputs")
	}
})

