let login = document.querySelector("#login")
let logout = document.querySelector("#logout")
let register = document.querySelector("#register")
let profile = document.querySelector("#profile")
let adminUser = localStorage.getItem("isAdmin")

if (localStorage.getItem('token') === null) {

	register.innerHTML = `<a href="./pages/register.html" class="nav-link active"> Register </a>`
	login.innerHTML = `<a href="./pages/login.html" class="nav-link active"> Log in </a>`
} else {
	if (adminUser === 'true') {
		profile.innerHTML =
			logout.innerHTML = `<a href="./pages/logout.html" class="nav-link active"> Log Out </a>`
	} else {
		profile.innerHTML = `<a href="./pages/profile.html" class="nav-link active"> Profile </a>`
		logout.innerHTML = `<a href="./pages/logout.html" class="nav-link active"> Log Out </a>`
	}

}