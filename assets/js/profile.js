
let token = localStorage.getItem("token");
let myName = document.querySelector("#myName");
let myNumber = document.querySelector("#myNumber");
let myEmail = document.querySelector("#myEmail");

fetch("https://capstonebookingapi.herokuapp.com/api/users/details", {
    method: "GET",
    headers: {
        Authorization: `Bearer ${token}`,
    },
})
    .then((res) => {
        return res.json();
    })
    .then((data) => {
        myName.innerHTML = data.firstname + " " + data.lastname;
        myNumber.innerHTML = data.mobileNo;
        myEmail.innerHTML = data.email;
        let courseData
        let courseData2 = data.enrollments
        courseData2.map((course) => {
            fetch(`https://capstonebookingapi.herokuapp.com/api/courses/${course.courseId}`, {
                method: "GET",
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            })
                .then((res) => res.json())
                .then((data) => {
                    courseData = data
                    courseData =
                        (`
                        <div class="col-md-6 my-3">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">${courseData.name}</h5>
                                    <p class="card-text text-left">${courseData.description}</p>
                                    <p class="card-text text-right">${courseData.price}</p>
                                </div>
                            </div>
                        </div>
                        `)
                    container.innerHTML += courseData
                })
            let container = document.querySelector("#coursesContainer");
        })
    })
